package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-09-17T23:07:16.959+0200")
@StaticMetamodel(TeamsEntity.class)
public class TeamsEntity_ {
	public static volatile SingularAttribute<TeamsEntity, Integer> id;
	public static volatile SingularAttribute<TeamsEntity, String> name;
	public static volatile SingularAttribute<TeamsEntity, String> description;
	public static volatile SetAttribute<TeamsEntity, UserEntity> users;
}

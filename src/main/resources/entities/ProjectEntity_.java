package entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-09-17T23:07:16.832+0200")
@StaticMetamodel(ProjectEntity.class)
public class ProjectEntity_ {
	public static volatile SingularAttribute<ProjectEntity, int> id;
	public static volatile SingularAttribute<ProjectEntity, String> name;
	public static volatile SingularAttribute<ProjectEntity, String> description;
	public static volatile SingularAttribute<ProjectEntity, Date> startDate;
	public static volatile SingularAttribute<ProjectEntity, Date> dueDate;
	public static volatile SingularAttribute<ProjectEntity, PrioritiesEntity> priority;
	public static volatile SingularAttribute<ProjectEntity, TeamsEntity> team;
}

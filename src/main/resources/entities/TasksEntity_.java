package entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-09-17T23:07:16.855+0200")
@StaticMetamodel(TasksEntity.class)
public class TasksEntity_ {
	public static volatile SingularAttribute<TasksEntity, Integer> taskId;
	public static volatile SingularAttribute<TasksEntity, String> name;
	public static volatile SingularAttribute<TasksEntity, Date> startDate;
	public static volatile SingularAttribute<TasksEntity, Date> dueDate;
	public static volatile SingularAttribute<TasksEntity, ProjectEntity> project;
	public static volatile SingularAttribute<TasksEntity, PrioritiesEntity> priority;
	public static volatile SingularAttribute<TasksEntity, TypesEntity> type;
	public static volatile SingularAttribute<TasksEntity, UserEntity> assignee;
}

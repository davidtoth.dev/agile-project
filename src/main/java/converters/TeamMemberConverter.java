package converters;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import entities.UserEntity;
import services.UserService;

@FacesConverter(value = "memberConverter")
public class TeamMemberConverter implements Converter {
	
	@EJB private UserService userService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return userService.readById(Long.valueOf(value));
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof UserEntity) {
			return ((UserEntity) value).getFirstName() + " " + ((UserEntity) value).getLastName(); 
		} else {
			throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, value + "is not a valid User", null));
		}
		
		  
	}
	
}

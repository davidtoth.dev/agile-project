package beans;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import entities.ProjectEntity;
import entities.TeamEntity;
import services.ProjectService;
import services.TeamService;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Named("projectUpdateBacking")
@ViewScoped
public class ProjectUpdateBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String description;
	private LocalDate startDate;
	private LocalDate dueDate;
	private ProjectEntity project;
	private Long selectedTeamId;
	private List<TeamEntity> teams;

	@Inject private ProjectService projectService;
	@Inject private TeamService teamService;
	
	@PostConstruct
	public void init() {
		teams = teamService.readAll();
		
		if (id != null) {
			project = projectService.readById(id);
			name = project.getName();
			description = project.getDescription();
			startDate = project.getStartDate();
			dueDate = project.getDueDate();
			if (project.getTeam() == null) {
				selectedTeamId = 0L;
			} else {
				selectedTeamId = project.getTeam().getId();
			}
		}
	}
	
	public void update() {
		ProjectEntity projectToUpdate = project;
		projectToUpdate.setName(name);
		projectToUpdate.setDescription(description);
		projectToUpdate.setStartDate(startDate);
		projectToUpdate.setDueDate(dueDate);
		projectToUpdate.setTeam(teamService.readById(selectedTeamId));
		
		try {
			projectService.update(projectToUpdate);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Project updated successfuly", null));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Project update failed", null));
			return;
		}
	}

	public ProjectEntity getProject() {
		return project;
	}
	
	public void setProject(ProjectEntity project) {
		this.project = project;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Long getSelectedTeamId() {
		return selectedTeamId;
	}

	public void setSelectedTeamId(Long selectedTeamId) {
		this.selectedTeamId = selectedTeamId;
	}

	public List<TeamEntity> getTeams() {
		return teams;
	}
	
}

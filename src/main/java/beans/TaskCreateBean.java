package beans;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import entities.PriorityEntity;
import entities.ProjectEntity;
import entities.StatusEntity;
import entities.TaskEntity;
import entities.UserEntity;
import services.PriorityService;
import services.ProjectService;
import services.StatusService;
import services.TaskService;
import services.UserService;
import java.io.Serializable;

@Named("taskBacking")
@ViewScoped
public class TaskCreateBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private LocalDate startDate;
	private LocalDate dueDate;
	private Long projectId;
	private Long priorityId;
	private Long statusId;
	private Long assigneeId;
	
	private String newPriorityName;
	
	private List<ProjectEntity> projects;
	private List<PriorityEntity> priorities;
	private List<UserEntity> assignees;
	private List<StatusEntity> statuses;
	
	private TaskEntity task;
	
	@EJB private TaskService taskService;
	@EJB private ProjectService projectService;
	@EJB private PriorityService priorityService;
	@EJB private UserService userService;
	@EJB private StatusService statusService;
	
	@PostConstruct
	public void init() {
		projects = projectService.readAll();
		priorities = priorityService.readAll();
		assignees = userService.readAll();
		statuses = statusService.readAll();
	}
	
	public void createNewTask() {
		task = new TaskEntity();
		task.setName(name);
		task.setStartDate(startDate);
		task.setDueDate(dueDate);
		task.setProject(projectService.readById(projectId));
		task.setPriority(priorityService.readById(priorityId));
		task.setStatus(statusService.readById(statusId));
		task.setAssignee(userService.readById(assigneeId));
		
		try {
			taskService.create(task);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "New task created successfully."));
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", "A problem occuried during task creation."));
			e.printStackTrace();
		}
	}
	
	public void onNameChange() {
		try {
			TaskEntity newTask = taskService.readByName(name);
			if (newTask != null) {
				FacesContext.getCurrentInstance().addMessage("newTaskForm:taskName", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Naming error", "Name already exists, try another one."));
			} else {
				return;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onStartDateSelect(SelectEvent<LocalDate> event) {
		if (startDate.isBefore(LocalDate.now())) {
			FacesContext.getCurrentInstance().addMessage("newTaskForm:startDate", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Start date cannot be before today", null));
		}
	}
	
	public void onDueDateSelect(SelectEvent<LocalDate> event) {
		if (startDate.isBefore(LocalDate.now())) {
			FacesContext.getCurrentInstance().addMessage("newTaskForm:dueDate", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Due date cannot be in the past", null));
		} else if (dueDate.isBefore(startDate)) {
			FacesContext.getCurrentInstance().addMessage("newTaskForm:dueDate", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong due date", "Due date cannot be before start date."));
		}
	}
	
	public void createNewPriority() {
		PriorityEntity newPriority = new PriorityEntity();
		if (priorityService.readByName(newPriorityName) != null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Priority error", "Priority name already exists."));
			return;
		} else {
			newPriority.setName(newPriorityName);
			priorityService.create(newPriority);
			priorities.add(newPriority);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}
	
	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getNewPriorityName() {
		return newPriorityName;
	}

	public void setNewPriorityName(String newPriorityName) {
		this.newPriorityName = newPriorityName;
	}

	public List<ProjectEntity> getProjects() {
		return projects;
	}

	public List<PriorityEntity> getPriorities() {
		return priorities;
	}

	public List<UserEntity> getAssignees() {
		return assignees;
	}

	public List<StatusEntity> getStatuses() {
		return statuses;
	}
	
}

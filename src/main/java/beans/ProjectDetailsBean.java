package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import entities.ProjectEntity;
import entities.TaskEntity;
import services.ProjectService;
import services.TaskService;

import java.io.Serializable;
import java.util.List;

@Named("projectDetailsBacking")
@ViewScoped
public class ProjectDetailsBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private ProjectEntity project;
	private List<TaskEntity> tasks;
	
	@EJB private ProjectService projectService;
	@EJB private TaskService taskService;
	
	@PostConstruct
	public void init() {
		if (name != null) {
			project = projectService.readByName(name);
			tasks = taskService.readForProject(project.getId());
		}
		
		if (project == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unknown project", null));
			return;
		}
	}
	
	public String delete() {
		try {
			projectService.delete(project);
			return "projects.xhtml?faces-redirect=true"; 
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error while deleting project", null));
			return null;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProjectEntity getProject() {
		return project;
	}

	public List<TaskEntity> getTasks() {
		return tasks;
	}
	
}

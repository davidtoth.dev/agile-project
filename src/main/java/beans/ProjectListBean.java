package beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import entities.ProjectEntity;
import services.ProjectService;
import services.TaskService;

@Named("projectList")
@RequestScoped
public class ProjectListBean {

	private List<ProjectEntity> projects;
	private double projectTasks;
	private double doneTasks;
	
	@EJB private ProjectService projectService;	
	@EJB private TaskService taskService;
	
	@PostConstruct
	public void init() {
		projects = projectService.readAll();
	}
	
	public double calcProgress(Long pid) {
		projectTasks = taskService.countAllForProject(pid);
		doneTasks = taskService.countDoneForProject(pid);
		
		double result = 0;
		
		if (projectTasks != 0) {
			result = Math.round((doneTasks / projectTasks) * 100);
		}
		
		return result;
	}

	public List<ProjectEntity> getProjects() {
		return projects;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
	
}

package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.CellEditEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;

import entities.StatusEntity;
import entities.TaskEntity;
import entities.UserEntity;
import services.StatusService;
import services.TaskService;
import services.TeamService;
import services.UserService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named("dashboardBacking")
@ViewScoped
public class DashboardBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long taskId;

	private List<String> teamsOfUser;
	private List<TaskEntity> tasksOfUser;
	private List<UserEntity> teamMembers;
	private List<StatusEntity> allStatuses;
	
	private DonutChartModel donutModel;
	private UserEntity loggedInUser;
	
	@EJB private TaskService taskService;
	@EJB private UserService userService;
	@EJB private StatusService statusService;
	@EJB private TeamService teamService;
	
	private Map<String, Integer> statusCount = new HashMap<String, Integer>();
	
	@PostConstruct
	public void init() {
		loggedInUser = (UserEntity) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loggedInUser");
		Long loggedInIUserId = loggedInUser.getId();
		
		teamsOfUser = userService.readForUser(loggedInIUserId);
		tasksOfUser = taskService.readForUser(loggedInIUserId);
		allStatuses = statusService.readAll();
		teamMembers = userService.readMembersForUser(loggedInIUserId);
		
		taskCounter();
		
		donutModel = new DonutChartModel();
		ChartData chartData = new ChartData();
		DonutChartDataSet chartDataSet = new DonutChartDataSet();
		
		// Set values for the donut chart
		List<Number> taskCount = new ArrayList<Number>(statusCount.values());
		chartDataSet.setData(taskCount);
		
		// Set the colors for the donut chart
		List<String> chartColors = new ArrayList<String>();
		chartColors.add("rgb(79, 99, 132)");
		chartColors.add("rgb(188, 71, 73)");
		chartColors.add("rgb(56, 102, 65)");
		chartColors.add("rgb(242, 232, 207)");
		chartColors.add("rgb(142, 142, 142)");
		chartDataSet.setBackgroundColor(chartColors);
		
		// Add data set to the chart data
		chartData.addChartDataSet(chartDataSet);
		
		// Set the labels for the donut chart
		List<String> chartLabels = new ArrayList<String>(statusCount.keySet());
		chartData.setLabels(chartLabels);
		
		// Add the data to the model
		donutModel.setData(chartData);
	}
	
	private void taskCounter() {
		List<String> statusNames = new ArrayList<String>();
		List<TaskEntity> tempTaskList = new ArrayList<TaskEntity>(tasksOfUser);
		
		for (StatusEntity status : allStatuses) {
			statusNames.add(status.getName());
		}
		
		for (int i = 0; i < statusNames.size(); i++) {
			String currentStatus = statusNames.get(i);
			int counter = 0;
			
			for (int j = 0; j < tempTaskList.size(); j++) {
				if (tempTaskList.get(j).getStatus() != null) {	
					String taskStatus = tempTaskList.get(j).getStatus().getName();
					
					if (taskStatus.equals(currentStatus)) {
						counter++;
						tempTaskList.remove(j);
					}
				}
			}
			
			statusCount.put(currentStatus, counter);
		}
	}
	
	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public List<String> getTeamsOfUser() {
		return teamsOfUser;
	}

	public List<UserEntity> getTeamMembers() {
		return teamMembers;
	}

	public DonutChartModel getDonutModel() {
		return donutModel;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public List<TaskEntity> getTasksOfUser() {
		return tasksOfUser;
	}

	public UserEntity getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(UserEntity loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public List<StatusEntity> getAllStatuses() {
		return allStatuses;
	}
	
}

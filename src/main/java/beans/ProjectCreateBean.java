package beans;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.primefaces.event.SelectEvent;

import entities.PriorityEntity;
import entities.ProjectEntity;
import services.PriorityService;
import services.ProjectService;

@Named("projectBacking")
@RequestScoped
public class ProjectCreateBean {
	
	private Long id;
	private String name;
	private String description;
	private LocalDate startDate;
	private LocalDate dueDate;
	private Long priorityId;
	private Long teamId;
	
	private List<PriorityEntity> priorities;
	
	private ProjectEntity newProject = new ProjectEntity();
	private String newPriorityName;
	
	@Inject
	private ProjectService projectService;
	
	@Inject
	private PriorityService priorityService;
	
	@PostConstruct
	public void init() {
		priorities = priorityService.readAll();
	}
	
	public void onStartDateSelect(SelectEvent<LocalDate> event) {
		if (startDate.isBefore(LocalDate.now())) {
			FacesContext.getCurrentInstance().addMessage("new-project:startDate", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Start date cannot be before today", null));
		}
	}
	
	public void onDueDateSelect(SelectEvent<LocalDate> event) {
		if (dueDate.isBefore(LocalDate.now())) {
			FacesContext.getCurrentInstance().addMessage("new-project:dueDate", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Due date cannot be before start date", null));
		}
	}
	
	public void createNewProject() {
		newProject.setName(name);
		newProject.setDescription(description);
		newProject.setStartDate(startDate);
		newProject.setDueDate(dueDate);
		try {
			projectService.create(newProject);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Project created successfully", null));
		} catch (PersistenceException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Project already exists", "Try an other name"));
			e.printStackTrace();
		}
	}
	
	public void createNewPriority() {
		PriorityEntity newPriority = new PriorityEntity();
		if (priorityService.readByName(newPriorityName) != null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Priority error", "Priority name already exists."));
			return;
		} else {
			newPriority.setName(newPriorityName);
			priorityService.create(newPriority);
			priorities.add(newPriority);
		}
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}
	
	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public List<PriorityEntity> getPriorities() {
		return priorities;
	}

	public void setPriorities(List<PriorityEntity> priorities) {
		this.priorities = priorities;
	}

	public String getNewPriorityName() {
		return newPriorityName;
	}

	public void setNewPriorityName(String newPriorityName) {
		this.newPriorityName = newPriorityName;
	}
	
}

package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityExistsException;

import entities.TeamEntity;
import entities.UserEntity;
import services.TeamService;
import services.UserService;

@Named("teamCreateBacking")
@ViewScoped
public class TeamCreateBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String description;
	private TeamEntity team;
	
	private List<UserEntity> selectedUsers;
	private List<UserEntity> users;
	private Set<UserEntity> members;
	
	@EJB private TeamService teamService;
	@EJB private UserService userService;
	
	@PostConstruct
	public void init() {
		users = userService.readAll();
		members = new HashSet<UserEntity>();
		team = new TeamEntity();
		selectedUsers = new ArrayList<UserEntity>();
	}
	
	public void createNewTeam() {
		
		for (UserEntity user : selectedUsers) {
			members.add(user);
		}
		
		team.setName(name);
		team.setDescription(description);
		team.setMembers(members);
		
		try {
			teamService.create(team);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Team created successfully"));
			
		} catch (EntityExistsException eee) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Team already exists", "Try another name"));
			eee.printStackTrace();
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal error", "Something went wrong while creating team."));
			e.printStackTrace();
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desctiption) {
		this.description = desctiption;
	}
	
	public List<UserEntity> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<UserEntity> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public List<UserEntity> getUsers() {
		return users;
	}
	
}

package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.UnselectEvent;

import entities.TeamEntity;
import entities.UserEntity;
import services.TeamService;
import services.UserService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Named("teamUpdateBacking")
@ViewScoped
public class TeamUpdateBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String description;
	private TeamEntity team;
	
	private List<UserEntity> users;
	private List<String> selectedUsers;
	private Set<UserEntity> members;
	
	@EJB private TeamService teamService;
	@EJB private UserService userService;

	@PostConstruct
	public void init() {
		users = userService.readAll();
		selectedUsers = new ArrayList<String>();
		members = new HashSet<UserEntity>();
		
		if (id != null) {
			team = teamService.readById(id);
			name = team.getName();
			description = team.getDescription();
			members = team.getMembers();
			
			for (UserEntity user : members) {
				selectedUsers.add(user.getEmail());
			}
		}
	}
	
	public void onItemUnselect(UnselectEvent<UserEntity> event) {
		TeamEntity teamToUpdate = team;
		members.remove(event.getObject());
		teamToUpdate.setMembers(members);
		teamService.update(teamToUpdate);
	}
	
	public void updateTeam() {
		TeamEntity teamToUpdate = team;
		members = new HashSet<UserEntity>();
		
		teamToUpdate.setName(name);
		teamToUpdate.setDescription(description);
		
		for (String selected : selectedUsers) {
			members.add(userService.readByEmail(selected));
		}
		
		teamToUpdate.setMembers(members);
		
		try {
			teamService.update(teamToUpdate);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Team updated successfully", null));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong on team update", null));
			e.printStackTrace();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TeamEntity getTeam() {
		return team;
	}

	public void setTeam(TeamEntity team) {
		this.team = team;
	}

	public List<UserEntity> getUsers() {
		return users;
	}

	public List<String> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<String> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}
	
}

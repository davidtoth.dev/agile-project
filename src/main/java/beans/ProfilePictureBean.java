package beans;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import services.UserService;

@Named("profilePicBacking")
@RequestScoped
public class ProfilePictureBean {
		
	@EJB
	private UserService userService;

	public StreamedContent getProfilePic() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else {
			String userEmail = context.getExternalContext().getRequestParameterMap().get("userEmail");
			byte[] userImage = userService.loadPhotoByEmail(userEmail);
			return new DefaultStreamedContent(new ByteArrayInputStream(userImage));
		}
	}
	
}

package beans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import entities.UserEntity;
import security.PasswordHasher;
import services.GroupService;
import services.UserService;

@Named("signup")
@RequestScoped
public class SignupBean {
	
	private String email;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private String confirmPassword;
	
	@EJB private UserService userService;
	
	@EJB private GroupService groupService;
	
	private UserEntity userToCreate;
	
	@Inject
	private PasswordHasher hasher;
	
	@PostConstruct
	public void init() {
		userToCreate = new UserEntity();
	}
	
	public String signup() {
		try {
			userToCreate = userService.readByEmail(email);
			
			// Check if user already exists with given email
			if (userToCreate.getEmail() == null) {
				userToCreate.setEmail(email);
				userToCreate.setFirstName(firstName);
				userToCreate.setLastName(lastName);
				
				// Checks if username is not already taken
				if (userToCreate.getUsername() == username || userToCreate.getUsername() != null) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username already in use!", "Please try an other username."));
					return null;
				} else {
					userToCreate.setUsername(username);
				}
				
				userToCreate.setPassword(hasher.generatePassword(password));
				userToCreate.setUserGroup(groupService.readById(2L));
				
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email already in use!", "Please use an other email"));
				return null;
			}
			
			userService.create(userToCreate);
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error while creating user", null));
			e.printStackTrace();
			return null;
		}
		
		return "signup-success.xhtml?faces-redirect=true";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserEntity getUserToCreate() {
		return userToCreate;
	}

	public void setUserToCreate(UserEntity userToCreate) {
		this.userToCreate = userToCreate;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
}

package beans;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.file.UploadedFile;

import entities.UserEntity;
import services.UserService;

import java.io.IOException;
import java.io.Serializable;

@Named("settingsBacking")
@ViewScoped
public class SettingsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UserService userService;
	
	private UploadedFile profilePic;
	
	private UserEntity loggedInUser;
	private Long userId;
	private String email;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	
	@PostConstruct
	public void init() {
		loggedInUser = (UserEntity) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loggedInUser");
		userId = loggedInUser.getId();
		firstName = loggedInUser.getFirstName();
		lastName = loggedInUser.getLastName();
		username = loggedInUser.getUsername();
		email = loggedInUser.getEmail();
		password = loggedInUser.getPassword();
	}
	
	public void updateUser() {
		loggedInUser.setFirstName(firstName);
		loggedInUser.setLastName(lastName);
		loggedInUser.setUsername(username);
		loggedInUser.setPassword(password);
		try {
			userService.update(loggedInUser);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Profile updated successfuly"));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", "Something went wrong. Please try again."));
			e.printStackTrace();
		}
		
	}
	
	public void upload() throws IOException {
		String fileName = profilePic.getFileName();
		
		loggedInUser.setProfilePhoto(profilePic.getContent());
		userService.update(loggedInUser);
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "New profile picture, " + fileName + " uploaded successfuly"));
	}

	public Long getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmial(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserEntity getLoggedInUser() {
		return loggedInUser;
	}

	public UploadedFile getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(UploadedFile profilePic) {
		this.profilePic = profilePic;
	}
	
}

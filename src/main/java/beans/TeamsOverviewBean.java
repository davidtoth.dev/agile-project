package beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import entities.ProjectEntity;
import entities.TeamEntity;
import entities.UserEntity;
import services.ProjectService;
import services.TeamService;
import services.UserService;

import java.io.Serializable;

@Named("teamsOverviewBacking")
@ViewScoped
public class TeamsOverviewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long selectedId;
	private TeamEntity selectedTeam;
	private List<TeamEntity> teams;
	private List<ProjectEntity> projects;
	private List<UserEntity> members;
	
	private boolean disabled;
	
	@EJB private TeamService teamService;
	@EJB private ProjectService projectService;
	@EJB private UserService userService;
	
	@PostConstruct
	public void init() {
		disabled = true;
		teams = teamService.readAll();
	}
	
	public void onTeamSelect() {
		selectedTeam = teamService.readById(selectedId);
		projects = projectService.readForTeam(selectedId);
		members = userService.readMemberForTeam(selectedId);
	}
	
	public void onItemSelect(SelectEvent event) {
		disabled = false;
	}
	
	public void deleteTeam() {
		if (selectedTeam != null) {
			try {	
				teamService.delete(selectedTeam);
				teams = teamService.readAll();
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Team: " + selectedTeam.getName() + " deleted successfuly", null));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error while deleting team", null));
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select a team for deletion", null));
		}
	}

	public List<TeamEntity> getTeams() {
		return teams;
	}

	public TeamEntity getSelectedTeam() {
		return selectedTeam;
	}

	public void setSelectedTeam(TeamEntity selectedTeam) {
		this.selectedTeam = selectedTeam;
	}

	public Long getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(Long selectedId) {
		this.selectedId = selectedId;
	}

	public List<ProjectEntity> getProjects() {
		return projects;
	}

	public List<UserEntity> getMembers() {
		return members;
	}
	
	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}

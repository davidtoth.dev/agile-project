package beans;

import java.io.IOException;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import entities.UserEntity;
import services.UserService;

import javax.security.enterprise.AuthenticationStatus;

@Named
@RequestScoped
public class LoginController {

	@NotNull
	private String email;
	
	@NotNull
	@Size(min = 6, message = "Password must be at least 6 characters")
	private String password;
	private UserEntity userToLogin;
	
	@Inject SecurityContext securityContext;
	@Inject FacesContext facesContext;
	@Inject ExternalContext externalContext;
	@EJB UserService userService;
	
	public void executeLogin() throws IOException {
		switch (processStatus()) {
		case SEND_CONTINUE:
			facesContext.responseComplete();
			break;
			
		case SEND_FAILURE:
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid username or password", null));
			break;
			
		case SUCCESS:
			userToLogin = userService.readByEmail(email);
			externalContext.getSessionMap().put("loggedInUser", userToLogin);
			externalContext.redirect(externalContext.getRequestContextPath() + "/user/dashboard.xhtml?faces-redirect=true");
			break;
			
		case NOT_DONE:
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong. Try again", null));
			break;
			
		}
	}
	
	private AuthenticationStatus processStatus() {
		return securityContext.authenticate(
				(HttpServletRequest) externalContext.getRequest(), 
				(HttpServletResponse) externalContext.getResponse(), 
				AuthenticationParameters.withParams().credential(new UsernamePasswordCredential(email, password)));
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
}

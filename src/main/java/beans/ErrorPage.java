package beans;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named("errorBacking")
@RequestScoped
public class ErrorPage {

	private int statusCode;
	private String error;
	
	FacesContext context;
	Map<String, Object> request;
	
	@PostConstruct
	public void init() {
		setErrorCode();
		setErrorMsg();
	}
	
	public void getRequest() {
		context = FacesContext.getCurrentInstance();
		request = context.getExternalContext().getRequestMap();
	}
	
	public void setErrorCode() {
		getRequest();
		statusCode = (int) request.get("javax.servlet.error.status_code");
	}
	
	public void setErrorMsg() {
		getRequest();
		error = request.get("javax.servlet.error.message").toString();
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getError() {
		return error;
	}
	
}

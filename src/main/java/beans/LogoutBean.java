package beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named("logout")
@RequestScoped
public class LogoutBean {
	
	@Inject HttpServletRequest request;

	public String logout() throws ServletException {
		request.logout();
		request.getSession().invalidate();
		return "/login.xhtml?faces-redirect=true";
	}

}

package beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import entities.PriorityEntity;
import entities.ProjectEntity;
import entities.StatusEntity;
import entities.TaskEntity;
import entities.UserEntity;
import services.PriorityService;
import services.ProjectService;
import services.StatusService;
import services.TaskService;
import services.UserService;

import java.io.Serializable;

@Named("taskListBacking")
@ViewScoped
public class TaskListBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<TaskEntity> taskList;
	private TaskEntity selectedTask;
	
	private boolean disabled;
	
	private Long projectId;
	private Long priorityId;
	private Long typeId;
	private Long assigneeId;
	private Long statusId;
	
	@EJB private TaskService taskService;
	@EJB private ProjectService projectService;
	@EJB private PriorityService priorityService;
	@EJB private UserService userService;
	@EJB private StatusService statusService;
	
	@PostConstruct
	public void init() {
		disabled = true;
		taskList = taskService.readAll();
	}
	
	public void onRowSelect(SelectEvent event) {
		disabled = false;
	}
	
	public void setParameters() {
		projectId = getSelectedTaskProject();
		priorityId = getSelectedTaskPriority();
		assigneeId = getSelectedTaskAssignee();
		statusId = getSelectedTaskStatus();
	}
	
	private Long getSelectedTaskProject() {
		ProjectEntity project = selectedTask.getProject();
		if (project == null) {
			return 0L;
		} else {
			return project.getId();
		}
	}
	
	private Long getSelectedTaskStatus() {
		StatusEntity status = selectedTask.getStatus();
		if (status == null) {
			return 0L;
		} else {
			return status.getId();
		}
	}
	
	private Long getSelectedTaskPriority() {
		PriorityEntity priority = selectedTask.getPriority();
		if (priority == null) {
			return 0L;
		} else {
			return priority.getId();
		}
	}
	
	private Long getSelectedTaskAssignee() {
		UserEntity assignee = selectedTask.getAssignee();
		if (assignee == null) {
			return 0L;
		} else {
			return assignee.getId();
		}
	}
	
	public void update() {
		try {
			selectedTask.setProject(projectService.readById(projectId));
			selectedTask.setPriority(priorityService.readById(priorityId));
			selectedTask.setAssignee(userService.readById(assigneeId));
			selectedTask.setStatus(statusService.readById(statusId));
			taskService.update(selectedTask);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Task updated successfuly."));
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failure", "Something went wrong during task update."));
			e.printStackTrace();
		}
	}
	
	public void save(TaskEntity taskToSave) {
		taskService.create(taskToSave);
		init();
	}
	
	public void delete() {
		taskService.delete(selectedTask);
		init();
	}

	public List<TaskEntity> getTaskList() {
		return taskList;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public TaskEntity getSelectedTask() {
		return selectedTask;
	}

	public void setSelectedTask(TaskEntity selectedTask) {
		this.selectedTask = selectedTask;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getPriorityId() {
		return priorityId;
	}

	public void setPriorityId(Long priorityId) {
		this.priorityId = priorityId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public Long getAssigneeId() {
		return assigneeId;
	}

	public void setAssigneeId(Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
}

package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import entities.PriorityEntity;

@Stateless
public class PriorityService {

	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;
	
	public PriorityEntity create(PriorityEntity priorityToCreate) {
		manager.persist(priorityToCreate);
		return priorityToCreate;
	}
	
	public PriorityEntity readById(Long id) {
		return manager.find(PriorityEntity.class, id);
	}
	
	public PriorityEntity readByName(String name) {
		try {
			return manager.createQuery("SELECT p FROM PriorityEntity p WHERE p.name = :name", PriorityEntity.class).setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			nre.printStackTrace();
			return null;
		}
	}
	
	public PriorityEntity readByLevel(Integer level) {
		return manager.createQuery("SELECT p FROM PriorityEntity p WHERE p.level = :level", PriorityEntity.class).setParameter("level", level).getSingleResult();
	}
	
	public List<PriorityEntity> readAll() {
		return manager.createQuery("SELECT p FROM PriorityEntity p", PriorityEntity.class).getResultList(); 
	}
	
	public void update(PriorityEntity priority) {
		manager.merge(priority);
	}
	
	public void delete(PriorityEntity priority) {
		manager.remove(manager.contains(priority) ? priority : manager.merge(priority));		
	}

}

package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.StatusEntity;

@Stateless
public class StatusService {
	
	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;
	
	public StatusEntity create(StatusEntity statusToCreate) {
		manager.persist(statusToCreate);
		return statusToCreate;
	}
	
	public StatusEntity readById(Long id) {
		return manager.find(StatusEntity.class, id);
	}
	
	public StatusEntity readByName(String name) {
		return manager.createQuery("SELECT s FROM StatusEntity s WHERE s.name = :name", StatusEntity.class).setParameter("name", name).getSingleResult();
	}
	
	public List<StatusEntity> readAll() {
		return manager.createQuery("SELECT s FROM StatusEntity s", StatusEntity.class).getResultList();
	}
	
	public void update(StatusEntity statusToUpdate) {
		manager.merge(statusToUpdate);
	}
	
	public void delete(StatusEntity statusToDelete) {
		manager.remove(manager.contains(statusToDelete) ? statusToDelete : manager.merge(statusToDelete));
	}
}

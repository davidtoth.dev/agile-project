package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.ProjectEntity;

@Stateless
public class ProjectService {

	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;
	
	public ProjectEntity create(ProjectEntity project) {
		manager.persist(project);
		return project;
	}
	
	public ProjectEntity readById(Long id) {
		return manager.find(ProjectEntity.class, id);
	}
	
	public ProjectEntity readByName(String name) {
		return manager.createQuery("SELECT p FROM ProjectEntity p WHERE p.name = :name", ProjectEntity.class)
				.setParameter("name", name)
				.getSingleResult();
	}
	
	public List<ProjectEntity> readAll() {
		return manager.createQuery("SELECT p FROM ProjectEntity p", ProjectEntity.class).getResultList();
	}
	
	public List<ProjectEntity> readForTeam(Long teamId) {
		return manager.createQuery("SELECT p FROM ProjectEntity p JOIN p.team t WHERE t.id = :teamId", ProjectEntity.class)
				.setParameter("teamId", teamId)
				.getResultList();
	}
	
	public void update(ProjectEntity project) {
		manager.merge(project);
	}
	
	public void delete(ProjectEntity project) {
		manager.remove(manager.contains(project) ? project : manager.merge(project));
	}
	
}

package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import entities.UserEntity;

@Stateless
public class UserService {
	
	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;
	
	public UserEntity create(UserEntity userToCreate) {
		manager.persist(userToCreate);
		return userToCreate;
	}
	
	public UserEntity readById(Long id) {
		return manager.find(UserEntity.class, id);
	}
	
	public UserEntity readByEmail(String email) {
		try {
			return manager.createQuery("SELECT u FROM UserEntity u WHERE u.email = :email", UserEntity.class).setParameter("email", email).getSingleResult();
		} catch (NoResultException e) {
			return new UserEntity();
		}
	}
	
	public List<String> readForUser(Long userId) {
		return manager.createQuery("SELECT t.name FROM UserEntity u JOIN u.teams t WHERE u.id = :userId", String.class).setParameter("userId", userId).getResultList();
	}
	
	public List<UserEntity> readAll() {
		return manager.createQuery("SELECT u FROM UserEntity u", UserEntity.class).getResultList();
	}
	
	public List<UserEntity> readMembersForUser(Long userId) {
		return manager.createQuery("SELECT tm FROM UserEntity u JOIN u.teams t, IN(t.members) tm WHERE u.id = :userId AND tm.id <> :userId", UserEntity.class)
				.setParameter("userId", userId)
				.getResultList();
	}
	
	public List<UserEntity> readMemberForTeam(Long teamId) {
		return manager.createQuery("SELECT tm FROM UserEntity u JOIN u.teams t, IN(t.members) tm WHERE u.id = tm.id AND t.id = :teamId", UserEntity.class)
				.setParameter("teamId", teamId)
				.getResultList();
	}
	
	public void update(UserEntity userToUpdate) {
		manager.merge(userToUpdate);
	}
	
	public void delete(UserEntity userToDelete) {
		manager.remove(manager.contains(userToDelete) ? userToDelete : manager.merge(userToDelete));
	}
	
	public byte[] loadPhotoByEmail(String email) {
		return manager.createQuery("SELECT u.profilePhoto FROM UserEntity u WHERE u.email = :email", byte[].class).setParameter("email", email).getSingleResult();
	}

}

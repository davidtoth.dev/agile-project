package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.TaskEntity;

@Stateless
public class TaskService {
	
	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;

	public void create(TaskEntity task) {
		manager.persist(task);
	}

	public TaskEntity readById(Long id) {
		return manager.find(TaskEntity.class, id);
	}

	public TaskEntity readByName(String name) {
		return manager.createQuery("SELECT t FROM TaskEntity t WHERE t.name = :name", TaskEntity.class)
				.setParameter("name", name)
				.getSingleResult();
	}
	
	public List<TaskEntity> readForUser(Long userId) {
		return manager.createQuery("SELECT t FROM TaskEntity t WHERE t.assignee.id = :userId", TaskEntity.class)
				.setParameter("userId", userId)
				.getResultList();
	}
	
	public double countAllForProject(Long pid) {
		return manager.createQuery("SELECT t FROM TaskEntity t WHERE t.project.id = :pid", TaskEntity.class)
				.setParameter("pid", pid)
				.getResultList()
				.size();
	}
	
	public double countDoneForProject(Long pid) {
		return manager.createQuery("SELECT t FROM TaskEntity t JOIN t.status ts WHERE ts.name = 'done' AND t.project.id = :pid", TaskEntity.class)
				.setParameter("pid", pid)
				.getResultList()
				.size();
	}
	
	public double countNotDoneForProject(Long pid) {
		return manager.createQuery("SELECT t FROM TaskEntity t JOIN t.status ts WHERE ts.name <> 'done' AND t.project.id = :pid", TaskEntity.class)
				.setParameter("pid", pid)
				.getResultList()
				.size();
	}
	
	public List<TaskEntity> readForProject(Long pid) {
		return manager.createQuery("SELECT t FROM TaskEntity t WHERE t.project.id = :pid", TaskEntity.class)
				.setParameter("pid", pid)
				.getResultList();
	}
	
	public List<TaskEntity> readAll() {
		return manager.createQuery("SELECT t FROM TaskEntity t", TaskEntity.class).getResultList();
	}

	public void update(TaskEntity task) {
		manager.merge(task);
	}

	public void delete(TaskEntity task) {
		manager.remove(manager.contains(task) ? task : manager.merge(task));
	}
	
}

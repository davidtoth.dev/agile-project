package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.TeamEntity;

@Stateless
public class TeamService {

	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;
	
	public TeamEntity create(TeamEntity team) {
		manager.persist(team);
		return team;
	}
	
	public TeamEntity readById(Long id) {
		return manager.find(TeamEntity.class, id);
	}
	
	public TeamEntity readByName(String name) {
		return manager.createQuery("SELECT t FROM TeamEntity t WHERE t.name = :name", TeamEntity.class).setParameter("name", name).getSingleResult();
	}
	
	public List<TeamEntity> readAll() {
		return manager.createQuery("SELECT t FROM TeamEntity t", TeamEntity.class).getResultList();
	}
	
	public void update(TeamEntity team) {
		manager.merge(team);
	}
	
	public void delete(TeamEntity team) {
		manager.remove(manager.contains(team) ? team : manager.merge(team));
	}

}

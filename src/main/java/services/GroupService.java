package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.UserGroupEntity;

@Stateless
public class GroupService {
	
	@PersistenceContext(unitName = "agilePU")
	private EntityManager manager;

	public UserGroupEntity create(UserGroupEntity groupToCreate) {
		manager.persist(groupToCreate);
		return groupToCreate;
	}
	
	public UserGroupEntity readById(Long groupId) {
		return manager.createQuery("SELECT ug FROM UserGroupEntity ug WHERE ug.id = :groupId ", UserGroupEntity.class)
				.setParameter("groupId", groupId)
				.getSingleResult();
	}
	
	public List<UserGroupEntity> readAll() {
		return manager.createQuery("SELECT ug FROM UserGroupEntity ug", UserGroupEntity.class).getResultList();
	}
	
	public void update(UserGroupEntity groupToUpdate) {
		manager.merge(groupToUpdate);;
	}
	
	public void delete(UserGroupEntity groupToDelete) {
		manager.remove(manager.contains(groupToDelete) ? groupToDelete : manager.merge(groupToDelete));
	}
	
}

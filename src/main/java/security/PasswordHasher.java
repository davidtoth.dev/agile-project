package security;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;

@Singleton
@Startup
public class PasswordHasher {

	@Inject
	private Pbkdf2PasswordHash pwdHasher;
	
	@PostConstruct
	public void init() {
		Map<String, String> hashParams = new HashMap<String, String>();
		hashParams.put("Pbkdf2PasswordHash.Iterations", "3072");
		hashParams.put("Pbkdf2PasswordHash.Algorithm", "PBKDF2WithHmacSHA512");
		hashParams.put("Pbkdf2PasswordHash.SaltSizeBytes", "64");
		pwdHasher.initialize(hashParams);
	}
	
	public String generatePassword(String password) {
		return pwdHasher.generate(password.toCharArray());
	}
	
	public PasswordHasher() {
		super();
	}
	
}

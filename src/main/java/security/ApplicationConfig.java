package security;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.inject.Named;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;

@DatabaseIdentityStoreDefinition(
		dataSourceLookup = "jdbc/agileProjectDS",
		callerQuery = "SELECT password FROM users WHERE email = ?",
		groupsQuery = "SELECT name FROM user_group ug JOIN users u ON ug.group_id = u.user_group WHERE u.email = ?",
		hashAlgorithm = Pbkdf2PasswordHash.class,
		priorityExpression = "#{100}",
		hashAlgorithmParameters = {
				"Pbkdf2PasswordHash.Iterations=3072",
				"Pbkdf2PasswordHash.Algorithm=PBKDF2WithHmacSHA512",
				"Pbkdf2PasswordHash.SaltSizeBytes=64"
		}
)
@CustomFormAuthenticationMechanismDefinition(
		loginToContinue = @LoginToContinue(
				loginPage = "/login.xhtml",
				useForwardToLogin = false,
				errorPage = ""
		)
)
@FacesConfig
@ApplicationScoped
@Named
public class ApplicationConfig {
	
}

package entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "status")
public class StatusEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "status_id")
	private Long id;
	
	@Column(nullable = false, unique = true)
	private String name;
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StatusEntity() {
		super();
	}
	
}

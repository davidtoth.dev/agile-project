package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "teams")
public class TeamEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "team_id", nullable = false)
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column
	private String description;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "team_members",
		joinColumns = { @JoinColumn(name = "team_id") },
		inverseJoinColumns = { @JoinColumn(name = "user_id") })
	private Set<UserEntity> members = new HashSet<UserEntity>();
	
	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Set<UserEntity> getMembers() {
		return members;
	}

	public void setMembers(Set<UserEntity> members) {
		this.members = members;
	}

	public TeamEntity() {
		super();
	}
	
	public TeamEntity(String name) {
		this.name = name;
	}
   
}

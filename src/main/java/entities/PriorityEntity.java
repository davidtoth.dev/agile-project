package entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "priorities")
public class PriorityEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "priority_id")
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public PriorityEntity() {
		super();
	}
   
}
